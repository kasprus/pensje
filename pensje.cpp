#include <iostream>
#include <cstdio>
#include <list>

using namespace std;

/*
 *
 */

void wczytaj(int * wartosci, list<int> * krawedzie, int n, int &liczba_pensji, bool *dostepne_pensje, int *pensja_pracownik, int *wartosci_mniejsze){
    //cin>>n;
    //wartosci = new int[n+1];
		wartosci[0]=0;
  //  cout<<wartosci;
    //krawedzie = new list<int> [n+1];
    //dostepne_pensje=new bool[n+1];
    int x, y;
    liczba_pensji=0;
    for(int a=1; a<=n; ++a){
        //cin>>x>>y;
				scanf("%d%d", &x, &y);
				pensja_pracownik[y]=a;
        if(x!=a)krawedzie[x].push_back(a);
				if(x==a)y=n;
        wartosci[a]=y;
        if(y==0)++liczba_pensji;
        else{
            dostepne_pensje[y]=true;
        }
    }
		for(int a=2; a<=n; ++a){
			wartosci_mniejsze[a]=wartosci_mniejsze[a-1]+!dostepne_pensje[a-1];
			//cout<<wartosci_mniejsze[a]<<endl;
		}
	//	cout<<"-------------\n";
}

void utworzZbior(int *zbior, int liczba_pensji, bool *dostepne_pensje, int n){
    //zbior=new int[liczba_pensji];
    int wsk=0;
    for(int a=1; a<=n; ++a){
        if(!dostepne_pensje[a])zbior[wsk++]=a;
    }
}

inline void utworzZbiorDrzew(int *drzewa, int &liczba_drzew, int n, int * wartosci, list<int>* krawedzie, int *pensja_pracownik){
    liczba_drzew=0;
    for(int a=1; a<=n; ++a){
        if(pensja_pracownik[a]){
            bool k=false;
            for(list<int>::iterator i=krawedzie[pensja_pracownik[a]].begin(); i!=krawedzie[pensja_pracownik[a]].end(); ++i){
                if(!wartosci[*i]){
                    k=true;
                    break;
                }
            }
						//if(krawedzie[pensja_pracownik[a]].size()==0)k=true;
            if(k)drzewa[liczba_drzew++]=pensja_pracownik[a];
        }
    }


}

int wyznaczWielkoscDrzewa(int a, list<int>* krawedzie, int *wartosci){
	int k=1;
	for(list<int>::iterator i=krawedzie[a].begin(); i!=krawedzie[a].end(); ++i){
		if(!wartosci[*i])k+=wyznaczWielkoscDrzewa(*i, krawedzie, wartosci);
	}
	return k;
}

void nadajWartosci(int wierzcholek, int *wsk, list<int>* krawedzie, int *wartosci, int ile){
	//cout<<"dupa "<<wierzcholek<<" "<<ile<<endl;
	if(ile<=0)return;
	if(!wartosci[wierzcholek]){
		wartosci[wierzcholek]=*(wsk--);
		ile--;
	}
	int zlicz=0;
	for(list<int>::iterator i=krawedzie[wierzcholek].begin(); i!=krawedzie[wierzcholek].end(); ++i){
		if(!wartosci[*i])zlicz++;
	}
	if(zlicz==1){
		//return nadajWartosci(krawedzie[wierzcholek][0], wsk, krawedzie, wartosci, ile);
		for(list<int>::iterator i=krawedzie[wierzcholek].begin(); i!=krawedzie[wierzcholek].end(); ++i){
			if(!wartosci[*i])return nadajWartosci(*i, wsk, krawedzie, wartosci, ile);
		}
	}
}
bool dostepne_pensje[1000001];
int pensja_pracownik[1000001];
int wartosci[1000001];
list <int> krawedzie[1000001];
int zbior[1000001];
int drzewa[1000001];
int wartosci_mniejsze[1000001];
int wielkosc_drzewa[1000001];

int main(int argc, char** argv) {
	//ios_base::sync_with_stdio(0);
    int n;
		scanf("%d", &n);
	//	bool *dostepne_pensje=new bool[n+1];
	//	int *pensja_pracownik=new int[n+1]();
	//	int * wartosci=new int[n+1]();
	//	list<int>* krawedzie=new list<int>[n+1];
    int liczba_pensji;
  //  int* zbior=new int[n+1];
  //  int* drzewa=new int[n+1];
	//	int *wartosci_mniejsze=new int[n+1]();
	//	int* wielkosc_drzewa=new int[n+1];
    int liczba_drzew;
    wczytaj(wartosci, krawedzie, n, liczba_pensji, dostepne_pensje, pensja_pracownik, wartosci_mniejsze);
    utworzZbior(zbior, liczba_pensji, dostepne_pensje, n);
    utworzZbiorDrzew(drzewa, liczba_drzew, n, wartosci, krawedzie, pensja_pracownik);
		int aktualnyNumer=0;
	//	cout<<endl;
		//usun_krawedzie(krawedzie, wartosci, n);
	//cout<<"liczba drzew wynosi"<<liczba_drzew<<endl;
    for (int i=0; i<liczba_drzew; ++i){
			wielkosc_drzewa[i]=wyznaczWielkoscDrzewa(drzewa[i],krawedzie, wartosci)-1;
			//cout<<drzewa[i]<<" "<<wielkosc_drzewa[i]<<"\n";
			if(wartosci_mniejsze[wartosci[drzewa[i]]]-aktualnyNumer==wielkosc_drzewa[i])nadajWartosci(drzewa[i], zbior+aktualnyNumer+wielkosc_drzewa[i]-1, krawedzie, wartosci, wartosci_mniejsze[wartosci[drzewa[i]]]-(i!=0?wartosci_mniejsze[wartosci[drzewa[i-1]]]:0));
			aktualnyNumer+=wielkosc_drzewa[i];
		}

	/*	for(int i=liczba_drzew-1; i>=0; --i){
			nadajWartosci(drzewa[i], zbior+aktualnyNumer-1, krawedzie, wartosci, wartosci_mniejsze[drzewa[i]]-i!=0?wartosci_mniejsze[drzewa[i]]:0);
			aktualnyNumer-=wielkosc_drzewa[i];
		}*/
	/*	cout<<"-------pensje--------\n";
		for(int i=0; i<liczba_pensji; ++i){
			cout<<zbior[i]<<" ";
		}
		cout<<"\n";*/
		for(int i=1; i<=n; ++i){
			printf("%d\n", wartosci[i]);
			//cout<<wartosci[i]<<endl;
		}
	/*	delete[] dostepne_pensje;
		delete[] pensja_pracownik;
		delete[] wartosci;
		delete[] krawedzie;
		delete[] zbior;
		delete[] drzewa;
		delete[] wielkosc_drzewa;
		delete[] wartosci_mniejsze;*/
    return 0;
}
